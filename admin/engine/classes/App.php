<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 05.07.2016
 * Time: 17:44
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/engine/classes/App.php');

class Admin_app extends App {
    function __construct()
    {
        parent::__construct();
    }

    static function isLoggedIn(){
        if (isset($_SESSION['administration'])){
            return 1;
        } else {
            return 0;
        }
    }

    function logIn($data){
        $user = $this->db->get('*', 'admins',
            "WHERE `name` = '".$data['username']."' AND `password` = '".$data['pass']."'");
        if (is_object($user)){
            $_SESSION['administration'] = $user;
        }
    }

    public static function logOut(){
        unset($_SESSION['administration']);
    }
}