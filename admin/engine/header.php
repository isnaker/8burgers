<header id="start">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/admin">8burgers</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="/admin/orders">Заказы</a></li>
                                <li><a href="/admin/products">Продукты</a></li>
                                <li><a href="/admin/ingredients">Ингредиенты</a></li>
                                <li><a href="/admin/couriers">Курьеры</a></li>
                            </ul>

                            <ul class="nav navbar-nav navbar-right">
                                <li><small><a class="btn btn-sm btn-default navbar-btn" href="/admin/?logout">Выйти</a> </small></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div>
        </div>
    </div>
</header>