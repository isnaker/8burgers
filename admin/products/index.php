
<?php

include_once($_SERVER['DOCUMENT_ROOT']."/admin/engine/classes/App.php");
$app = new Admin_app();

if (Admin_app::isLoggedIn()){
    $header = "header.php";
    if (isset($_POST['prod_name']))
    {
        $input=$_POST;
        $id=$input['id'];
        $what_array=array("name","description","weight","price","rating","disabled");
        if($input['prod_dis']=="on")
        {
            $disabled="0";
        }
        else{
            $disabled="1";
        }
        $val_array=array($input['prod_name'],$input['prod_desc'],$input['prod_weight'],$input['prod_cost'],$input['prod_rait'],$disabled);
        $app->db->set($what_array,$val_array,"products","WHERE `id`='$id'");
        header("Location: /admin/products/?id=".$input['id']."&issaved=1");

    }
    if(isset($_GET['id'])){
        $id=$_GET['id'];
        $product=$app->db->get("*","products","WHERE `id`='$id'");
        $img=$app->db->get("*","products_images","WHERE product_id='$id' LIMIT 1");
        $product->img = $img->image_url;
        if(isset($_GET['issaved']))
        {
            $product->issaved=1;
        }
        else{
            $product->issaved=0;
        }
        $ingridients=$app->db->get("*","ingredients","");
        $prod_ingr=$app->db->get("*","product_ingredients","WHERE `product_id`='$id'");

        foreach($ingridients as $item)
        {
            $item->checked=1;
            foreach($prod_ingr as $item2)
            {

                if($item2->ingredient_id==$item->id)
                {
                    $item->checked=2;
                }
            }
        }
        $includable = array('/admin/pages/product.php');
    }
    else{
        $products=$app->db->get("*","products","");
        foreach ($products as $item){
            $img=$app->db->get("*","products_images","WHERE product_id='$item->id' LIMIT 1");
            $item->img = $img->image_url;
        }

        $includable = array('/admin/pages/products.php');
    }

} else {
    $includable = array('/admin/pages/login.php');
}




include_once($_SERVER['DOCUMENT_ROOT']."/admin/page_template_admin.php");



?>




