
<?php

include_once($_SERVER['DOCUMENT_ROOT']."/admin/engine/classes/App.php");
$app = new Admin_app();

if (Admin_app::isLoggedIn()){
    // logged user
    $header = "header.php";
    $orders=$app->db->get("*","orders","");
    foreach ($orders as $item){
        $state=$app->db->get("*","order_states","WHERE id='$item->state_id' LIMIT 1");
        $item->state_txt = $state->name;
        $item->state_class = $state->class;
        $item->total = 0;
    }

    $includable = array('/admin/pages/orders.php');

} else {
    // not
    $includable = array('/admin/pages/login.php');
}
include_once($_SERVER['DOCUMENT_ROOT']."/admin/page_template_admin.php");


?>




