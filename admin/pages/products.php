<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="/admin"> Главная</a></li>
                <li class="active">Продукты</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <table class='table table-bordered admin_table'>
                <?php foreach($products as $item){?>
                    <tr>
                        <td><img class="img_admin" src="/img/products/<?php echo $item->img?>" alt=""></td>
                        <td><?php echo $item->name ?></td>
                        <td><a href="/admin/products/?id=<?php echo $item->id ?>">
                                <i style="color: #2aabd2;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </td>
                        <td><i style="color: <?php if($item->disabled==0){echo "red";}else{echo "black";}?>" prod_id="<?php echo $item->id?>" dis_state="<?php echo $item->disabled; ?>" class="fa fa-power-off disabler" aria-hidden="true"></i></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>

</div>
