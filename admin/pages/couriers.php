<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="/admin"> Главная</a></li>
                <li class="active">Курьеры</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <table class='table table-bordered admin_table'>
                <?php foreach($couriers as $item){?>
                    <tr class="<?php if($item->free==1){ echo "left-border-green";}else{echo "left-border-red";}?>">
                        <td><?php echo  $item->id; ?></td>
                        <td> <i style="color: <?php if($item->free==1){ echo "#25c03c";}else{echo "#fa3a26";}?>" cour_id="<?php echo  $item->id; ?>" free="<?php echo $item->free ?>"
                                class="pointer cour_state fa <?php if($item->free==1){ echo "fa-user";}else{echo "fa-user-times";}?>"></i>
                            <?php if($item->free==1){ echo "Свободен";}else{echo "Занят";}?>

                        </td>
                        <td> <?php echo $item->name; echo "  "; echo $item->last_name;?> </td>
                        <td>
                            <i style="color: red;" class="pointer fa fa-times delete" aria-hidden="true"></i>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div class="col-md-6">
            <form method="post" action="/admin/couriers/index.php">
                <div class="row">
                    <div class="col-md-4">
                        <strong>Имя: </strong><input name="cour_name" type="text" class="form-control">
                    </div>
                    <div class="col-md-5">
                        <strong>Фамилия: </strong><input name="cour_lastname" type="text" class="form-control">
                    </div>
                    <div class="col-md-3">
                        <br><button type="submit" class="btn labels-blue labels save_prod"><i class="fa fa-plus" aria-hidden="true"></i> Добавить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>