<div class="container">
    <hr>
    <div class="row">
        <div class="col-md-4 col-md-offset-2 text-center">
            <img src="/img/logo.png">
            <p class="h2">Вход в систему</p>
        </div>
        <div class="col-md-3">
            <form method="post" action="/admin/">
                <input name="action" value="login" hidden="hidden">
                <label>Ваш логин</label>
                <input class="form-control" type="text" name="username" placeholder="логин">
                <label>Пароль</label>
                <input class="form-control" type="password" name="pass" placeholder="***">

                <button type="submit" class="font-opensans btn btn-primary btn-block">Войти</button>
            </form>
        </div>
    </div>
</div>