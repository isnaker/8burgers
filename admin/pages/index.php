
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <a href="/admin/orders" role="button" class="btn btn-danger btn-big ">Заказы <span value="0" class="order_counter badge"></a>
        </div>
        <div class="col-md-3">    <a href="/admin/products" role="button" class="btn btn-warning btn-big">Продукты</a></div>
        <div class="col-md-3">    <a href="/admin/ingredients" role="button" class="btn btn-info btn-big">Ингредиенты</a></div>
        <div class="col-md-3">    <a href="/admin/couriers" role="button" class="btn btn-primary btn-big">Курьеры</a></div>
    </div>
</div>
