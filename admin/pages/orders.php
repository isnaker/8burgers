
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="/admin"> Главная</a></li>
                <li class="active">Управление заказами</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="btn-group btn-group-sm">
                <button type="button" value="0" class="btn btn-default sort"><strong>Все </strong><span value="0" class="order_counter badge"></span></button>
                <button type="button" value="1" class="btn btn-danger text-shadow sort">Новый       <span value="1" class="order_counter badge"></span></button>
                <button type="button" value="6" class="btn btn-warning text-shadow sort">Принят      <span value="6" class="order_counter badge"></span></button>
                <button type="button" value="2" class="btn btn-success text-shadow sort">Выполняется <span value="2" class="order_counter badge"></span></button>
                <button type="button" value="3" class="btn btn-info text-shadow sort">Доставка    <span value="3" class="order_counter badge"></span></button>
                <button type="button" value="5" class="btn btn-primary text-shadow sort">Выполнен    <span value="5" class="order_counter badge"></span></button>
                <button type="button" value="4" class="btn btn-default   sort"><strong>Отменен</strong>    <span value="4" class="order_counter badge"></span></button>
                <button type="button" class="btn btn-success addNewOrder">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    Добавить заказ
                </button>
            </div>
        </div>

        <div class="col-md-3">
            <select class="form-control sort_days">
                <option value="1">За сутки</option>
                <option value="7">За неделю</option>
                <option value="30">За месяц</option>
            </select>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-4 prokrutka">
            <table class='table admin_table' id="order_table"></table>
        </div>
        <div class="col-md-8">
            <div id="details" class="details hidden">
                <div class="row">
                    <div class="col-md-4">
                        <button class="btn btn-default pointer openLogs"
                              data-toggle="modal" data-target="#logsModal">
                            <i class="fa fa-file-code-o" aria-hidden="true"></i> Лог заказа
                        </button>
                    </div>
                    <div class="col-md-8">
                        <p class="h4 mt-none">Детали заказа #<span id="order_id"></span>
                            <sup role="state" class="label"></sup></p>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Клиент: </strong><input type="text" value="Павел" role="client_name" class="form-control"><br>
                        <strong>Телефон: </strong><input type="text" role="client_pfone" value="+7(915135-75-27)" class="form-control"><br>
                        <strong>Курьер: </strong><select role="client_courier" class="form-control"></select>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-8">
                                <strong>Адрес: </strong>
                                <textarea type="text" rows="4" role="client_adres" class="form-control"></textarea>
                            </div>
                            <div class="col-md-4">
                                <strong>Время на путь: </strong>
                                <textarea type="text" rows="1" role="order_time_travel" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <strong>Сдача: </strong>
                                <input type="text" rows="1" role="client_sdacha" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <strong>Время заказа: </strong>
                                <input type="text" rows="1" role="order_time_to" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <strong>Начало готовки: </strong>
                                <input type="text" rows="1" role="order_time_cook" class="form-control">
                            </div>
                        </div>
                        <strong>Комментарий: </strong>
                        <textarea rows="7" type="text" role="client_сomment" class="form-control"></textarea>
                    </div>
                </div>
                <div class="row hidden">
                    <div class="col-md-4"><strong>Клиент: </strong><input type="text"  role="client_name" class="form-control"></div>
                    <div class="col-md-8"><strong>Адрес: </strong><textarea type="text" rows="4" role="client_adres" class="form-control"></textarea></div>
                    <div class="col-md-4"><strong>Телефон: </strong><input type="text" role="client_pfone"  class="form-control"></div>
                    <div class="col-md-8"><strong>Комментарий: </strong><textarea rows="3" type="text" role="client_сomment" class="form-control"></textarea></div>
                    <div class="col-md-4"><strong>Курьер: </strong>
                        <select role="client_courier" class="form-control"></select>
                    </div>
                </div>
                <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="h4">Cостав заказа:
                                <button class="btn btn-default btn-sm pointer add_product pull-right">
                                    <i class="fa redf fa-pencil"
                                       area-hidden="true" title="Добавить продукты"></i> Добавить продукт
                                </button>
                            </p>
                            <table id="order_items" class="table table-bordered"></table>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="h4">Адрес и путь:</p>
                        <div id="map" style="height: 150px"></div>
                    </div>
                </div>

                <div class="row" >
                    <div class="col-md-10">
                        <div class="btn-group" id="cat_button">
                            <button class="btn btn-danger" state_id="1"><i class="fa fa-check"  aria-hidden="true"></i> Новый</button>
                            <button class="btn btn-warning" state_id="6"><i class="fa fa-check"  aria-hidden="true"></i> Принят</button>
                            <button class="btn btn-success" state_id="2"><i class="fa fa-check"  aria-hidden="true"></i> Выполяется</button>
                            <button class="btn btn-info" state_id="3"><i class="fa fa-check"  aria-hidden="true"></i> Доставка</button>
                            <button class="btn btn-primary" state_id="5"><i class="fa fa-check"  aria-hidden="true"></i> Выполнен</button>
                            <button class="btn btn-default" state_id="4"><i class="fa fa-check"  aria-hidden="true"></i> Отменен</button>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-primary save btn-block"><i class="fa fa-safari"></i> Сохранить</button>
                    </div>
                </div>
            </div>


            <div id="newOrder" class="details hidden">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-8">
                        <p class="h3">Создание заказа</p>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4">
                        <strong>Клиент: </strong><input type="text" value="" role="client_name_add" class="form-control"><br>
                        <strong>Телефон: </strong><input type="tel" role="client_pfone_add" value="" class="form-control"><br>
                    </div>
                    <div class="col-md-8">
                        <strong>Адрес: </strong><textarea type="text" rows="4" role="client_adres_add" class="form-control"></textarea>
                        <strong>Комментарий: </strong><textarea rows="3" type="text" role="client_сomment_add" class="form-control"></textarea>
                    </div>
                </div>
                <br>
                <p class="h4">Cостав заказа:
                    <button class="btn btn-default btn-sm pointer add_product pull-right">
                        <i class="fa redf fa-pencil"
                           area-hidden="true" title="Добавить продукты"></i> Добавить продукт
                    </button>
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <table id="order_items_add" class="table table-bordered">

                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button class="btn btn-primary pull-right save_new_order">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
