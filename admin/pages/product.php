<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="/admin"> Главная</a></li>
                <li><a href="/admin/products">Продукты</a></li>
                <li class="active"><?php echo $product->name ?></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <img src="/img/products/<?php echo $product->img; ?>" class="img-responsive">
        </div>
        <div class="col-md-8">
            <div class="<?php if($product->issaved!=1){echo "hidden";}?>">

                <div class="row">
                    <div class="col-md-12">
                        <div style="color: #fff; padding: 15px;" class="labels-green">
                            Товар успешно сохранен!
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <form action="/admin/products/index.php" method="post">
                <input type="hidden" name="id" value="<?php echo $product->id; ?>" type="text">
            <div class="row">
                <div class="col-md-8"><strong>Название: </strong><input type="text" value="<?php echo $product->name; ?>" name="prod_name" role="prod_name" class="form-control"></div>
                <div class="col-md-4"><strong>Рейтинг: </strong><input type="number" max="10" min="0"  value="<?php echo $product->rating; ?>" name="prod_rait" role="prod_rait" class="form-control"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-8"><strong>Описание: </strong><textarea type="text" name="prod_desc" role="prod_desc" rows="4" class="form-control"><?php echo $product->description;?></textarea></div>
                <div class="col-md-4"><strong>Цена: </strong><input type="text" name="prod_cost"  value="<?php echo $product->price; ?>" role="prod_cost" class="form-control"></div>
                <div class="col-md-4"><strong>Вес: </strong><input type="text" name="prod_weight" value="<?php echo $product->weight; ?>" role="prod_weight" class="form-control"></div>
            </div>
            <div class="row">
                <div class="col-md-3"><br><button type="submit" class="btn labels-blue labels save_prod"><i class="fa fa-floppy-o" aria-hidden="true"></i> Сохранить</button></div>
                <div class="col-md-1">
                    <br>
                    <div class="labels labels-red">
                    <i style="color:#fff; display: inline;" class="fa fa-power-off" aria-hidden="true"></i><input name="prod_dis" type="checkbox" <?php if($product->disabled==0){echo "checked";} ?>>
                    </div>
                </div>
            </div>
            </form>
            <br>
            <p><strong>Ингридиенты: </strong></p>
            <?php foreach($ingridients as $item){ ?>
                <div style="display: inline;" class="col-md-4">
                  <span state="<?php if($item->checked==2){echo "1";}else{echo "0";}?>" ingr_id="<?php echo $item->id; ?>" prod_id="<?php echo $product->id; ?>" class="prod_ingr">
                      <i class="fa fa-circle-o <?php if($item->checked==2){echo "hidden";} ?>" aria-hidden="true"></i><i class="fa fa-check-circle-o <?php if($item->checked!=2){echo "hidden";} ?>" aria-hidden="true"></i>
                  </span>
                    <?php echo $item->name; ?>
                </div>
            <?php }?>
            <br>

        </div>
    </div>
</div>



