<div class="container">
    <div class="row">
        <div class="col-md-12">
            <ol class="breadcrumb">
                <li><a href="/admin"> Главная</a></li>
                <li class="active">Ингредиенты</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <table class='table table-bordered admin_table'>
                <?php foreach($ingredients as $item){?>
                    <tr>
                        <td><input ingr_id="<?php echo $item->id ?>" class="update_input update_input<?php echo $item->id ?>" type="text" value="<?php echo $item->name ?>"></td>
                        <td>
                            <i ingr_id="<?php echo $item->id ?>" class="pointer fa fa-floppy-o save_ingr" aria-hidden="true"></i>
                        </td>
                        <td>
                            <i ingr_id="<?php echo $item->id ?>" style="color: red;" class="pointer fa fa-times del_ingr" aria-hidden="true"></i>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div class="col-md-6">
            <form method="post" action="/admin/ingredients/index.php">
            <div class="row">
                <div class="col-md-9">
                    <strong>Название: </strong><input name="ingr_name" type="text" class="form-control">
                </div>
                <div class="col-md-3">
                    <br><button type="submit" class="btn labels-blue labels save_prod"><i class="fa fa-plus" aria-hidden="true"></i> Добавить</button>
                </div>
            </div>
            </form>
        </div>
    </div>

</div>