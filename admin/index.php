<?php
include_once($_SERVER['DOCUMENT_ROOT']."/admin/engine/classes/App.php");
$app = new Admin_app();

if (isset($_POST['action']) && $_POST['action'] == "login"){
    $app->logIn($_POST);
}

if (isset($_GET['logout'])){
    Admin_app::logOut();
    header("Location: /admin");
}

if (Admin_app::isLoggedIn()){
    // logged user
    $header = "header.php";
    $includable = array('/admin/pages/index.php');
    include_once($_SERVER['DOCUMENT_ROOT']."/admin/page_template_admin.php");
} else {
    // not
    $includable = array('/admin/pages/login.php');
    include_once($_SERVER['DOCUMENT_ROOT']."/admin/page_template_admin.php");
}



?>

