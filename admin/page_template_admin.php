<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>8burgers</title>
    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/admin/css/style.css" rel="stylesheet">
    <link href="/css/jquery.fancybox.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="admin">
<?php if (isset($header)){ include_once($_SERVER['DOCUMENT_ROOT']."/admin/engine/".$header); }  ?>


            <?php if (isset($content)) {print $content; } ?>
            <?php if (isset($includable)){
                foreach($includable as $item)
                {
                    include_once($_SERVER['DOCUMENT_ROOT'].$item);
                }
            } ?>


<div class="modal fade" id="orderModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p class="h5 modal-title"><strong>Добавление заказа</strong></p>
            </div>
            <div  class="modal-body">
                <div class="col-md-8"><strong>Адрес: </strong><textarea type="text" rows="4" role="client_adres" class="form-control"></textarea></div>
                <div class="col-md-4"><strong>Телефон: </strong><input type="text" role="client_pfone" value="+7(915135-75-27)" class="form-control"></div>
                <div class="col-md-8"><strong>Комментарий: </strong><textarea rows="3" type="text" role="client_сomment" class="form-control"></textarea></div>
            </div>
            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="logsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p class="h5 modal-title"><strong>Логи заказа</strong></p>
            </div>
            <div  class="modal-body">
                <table id="logList" class="table table-bordered"></table>
            </div>
            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="productComment" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p class="h5 modal-title"><strong>Комментарий продукта</strong></p>
            </div>
            <div id="prodComment" class="modal-body">
                    <div class="col-md-12">
                        <textarea rows="8" class="form-control"></textarea>
                    </div>
                <div class="btn-group btn-sm ">
                <button class="comment_add btn btn-default">
                    Full
                </button>

                <button class="comment_add btn btn-default">
                    Medium
                </button>

                <button class="comment_add btn btn-default">
                    +Картошка фри
                </button>
                    <button class="comment_add btn btn-default">
                        +Соус
                    </button>
                    </div>
            </div>
            <div class="modal-footer">
            <button id="buttonCommentSave" class="btn btn-primary">Сохранить</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery.min.js"></script>
<!--<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/slick.min.js"></script>



<script src="/js/wow.js"></script>
<script src="/js/jquery.easing.min.js"></script>
<script src="/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/sliders.js"></script>
<script src="/js/forms.js"></script>
<script src="/js/cart.js"></script>

<!--<script src="https://api-maps.yandex.ru/1.1/index.xml" type="text/javascript"></script>-->
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/admin/js/orders.js"></script>

</body>
</html>