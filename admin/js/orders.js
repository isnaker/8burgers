var Order = {};//Переменная содержащая обьект редактируемого заказа
var Orders=[];//Массив заказов
var Products=[];//Массив продуктов
var Current_days=1;//Кол-во суток по которым выводятся заказы
var Current_state=1;//Вкладка состояний
var Old_Orders=[];//Старые заказы
var isNew=0;
var Logs;//Логи
$(function(){

    $('<audio id="chatAudio"><source src="/admin/sound/order.ogg" type="audio/ogg"><source src="/admin/sound/order.mp3" type="audio/mpeg"><source src="/admin/sound/order.wav" type="audio/wav"></audio>').appendTo('body');


    getOrders(Current_days,false);
    setInterval("getOrders(Current_days)",15000);
    getProducts();

    //$('.prokrutka').css('height',$(document).height()-220);

    $(document).on('click','.addNewOrder',function(){
        $('#details').addClass('hidden');
        $('#newOrder').removeClass('hidden');
        Order={
            client: {
                name : "",
                phone : "",
                address : ""
            },
            comment : "",
            contents : []
        };
        $('#order_items_add').html(toOrderProductsView(Order.contents));
        $('#order_table').html(toProductsView(Products));
        Current_state=888;
    });
    $(document).on('click','.openLogs',function(){
        $('#logList').html(toLogsView(Logs));
    });
    $(document).on('click','.save_new_order',function(){
        Order.client.name=$('[role="client_name_add"]').val();
        Order.client.phone=$('[role="client_pfone_add"]').val();
        Order.client.address=$('[role="client_adres_add"]').val();
        Order.comment=$('[role="client_сomment_add"]').val();
        Order.contents=JSON.stringify(Order.contents);

        Order.time_travel = $('[role="order_time_travel"]').val();
        Order.time_to = $('[role="order_time_to"]').val();
        Order.time_cook = $('[role="order_time_cook"]').val();

        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'add_order',
                data: JSON.stringify(Order)
            },
            success: function(response){
                getOrders(Current_days);
                var resp = JSON.parse(response);
                setDetail(resp.response);
            }

        });
    });


    //Добавление продуктов в заказ//

    $(document).on('click','.add_prod_cart',function(){
        var that = this;
        var product=getProductById($(that).attr('prod_id'));
        product.count=1;
        var isDouble=false;
        for(var i=0;i<Order.contents.length;i++)
        {
            if(Order.contents[i].id==product.id){
                Order.contents[i].count++;
                isDouble=true;
            }
        }
        if(!isDouble){
            product.comment="";
            Order.contents.push(product);
        }
        $("#order_items,#order_items_add").html(toOrderProductsView(Order.contents));
        $('.comment').popover({
            trigger :'hover'
        });
    });

    $(document).on('click','.del_prod_cart',function(){
        var that = this;
        var product=$(that).attr('prod_id');
        for(var i=0;i<Order.contents.length;i++){
            if(Order.contents[i].id==product){
                Order.contents[i].count--;
                if(Order.contents[i].count<=0){
                    Order.contents.splice(i,1);
                }
            }
        }
        $("#order_items,#order_items_add").html(toOrderProductsView(Order.contents));
    });
    $(document).on('click','.comment',function(){
        var that = this;
        var texta = $('#productComment').find('textarea');
        $(texta).val(Order.contents[$(that).attr('data-prod-i')].comment);
        $('#productComment').modal('show');
        $('#buttonCommentSave').attr('data-prod-i',$(that).attr('data-prod-i'));
    });
    $(document).on('click','#buttonCommentSave',function(){
        var that = this;
        var text= $('#productComment').find('textarea').val();
        Order.contents[$(that).attr('data-prod-i')].comment=text;
        $("#order_items,#order_items_add").html(toOrderProductsView(Order.contents));
        $('.comment').popover({
            trigger :'hover'
        });
        $('#productComment').modal('hide');
    });
    $(document).on('click','.comment_add',function(){
        var that = this;
        var texta = $('#productComment').find('textarea');
        $(texta).val($(texta).val()+$(that).text());
    });
    ///////////////////////////////


    //
    $('.sort_days').change(function(){
        var days =$('.sort_days').val();
        Current_days=(days);
        getOrders(days,false);
    });
    $('.add_product').click(function(){
        Current_state=888;
        $('#order_table').html(toProductsView(Products));
    });
    //Изменение состава ингредиента
    $('.prod_ingr').click(function(){
        var that = this;
        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'set_product_ingredients',
                data: {
                    prod_id: $(that).attr('prod_id'),
                    ingr_id: $(that).attr('ingr_id'),
                    state: $(that).attr('state')
                }
            },
            success: function(response){
                if($(that).attr('state')=="1"){
                    $(that).attr('state',"0");
                }
                else{
                    $(that).attr('state',"1");
                }
                $(that).find('i').toggleClass('hidden');
            }

        });




    });

    //Выбор активного статуса заказа
    $("#cat_button button").click(function(){
        var that = this;
        var but = $("#cat_button").find('.active_cat');
        $(but).removeClass('active_cat');
        $(that).addClass('active_cat');
    });


    //Загрузка заказа в блок деталей
    $(document).on('click','.det',function(){
        if($(this).hasClass('new_order')) {
            $(this).removeClass('new_order');
            for (var i = 0; i < Orders.length; i++) {
                if (Orders[i].id == $(this).attr('data-id')) {
                    Orders[i].new = 0;
                }
            }
        }
        $('#details').removeClass('hidden');
        $('#newOrder').addClass('hidden');
        setDetail($(this).attr('data-id'));
    });

    //Сортировка заказов
    $(document).on('click','.sort',function(){
        var that = this;
        Current_state=$(that).val();
        $('#order_table').html(toOrderView(filtrOrders(Current_state,Orders)));
    });

    //Сохранение ингредиента
    $(document).on('click','.save_ingr',function(){
        var that=this;
        var id = ".update_input"+$(that).attr('ingr_id');
        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'update_ingredient',
                data: {
                    id : $(that).attr('ingr_id'),
                    name : $(id).val()
                }
            },
            success: function(response){
                alert("Имя сохранено");
            }

        });
    });

    //Изменение состояния курьра
    $(document).on('click','.cour_state',function(){
        var that = this;
        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'update_courier',
                data: {
                    id : $(that).attr('cour_id'),
                    free: $(that).attr('free')
                }
            },
            success: function(response){
                window.location.reload()
            }

        });
    });

    //Удаление ингредиента
    $(document).on('click','.del_ingr',function(){
        var that = this;
        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'delete',
                data: {
                    id : $(that).attr('ingr_id'),
                    table : "ingredients"
                }
            },
            success: function(response){
                alert("Ингредиент удален");
            }

        });
    });

    //Отправка заказа на обновление
    $(document).on('click','.save',function(){
        var id=Order.id;

        Order.client.name=$('[role="client_name"]').val();
        Order.client.phone=$('[role="client_pfone"]').val();
        Order.client.address=$('[role="client_adres"]').val();
        Order.comment=$('[role="client_сomment"]').val();
        Order.state_id=$('.active_cat').attr('state_id');
        Order.courier_id=$('[role="client_courier"]').val();
        Order.contents=JSON.stringify(Order.contents);
        Order.sdacha=$('[role="client_sdacha"]').val();

        Order.time_travel = $('[role="order_time_travel"]').val();
        Order.time_to = $('[role="order_time_to"]').val();
        Order.time_cook = $('[role="order_time_cook"]').val();

        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'update_order',
                data: JSON.stringify(Order)
            },
            success: function(response){
                getOrders(Current_days);
                setDetail(id);
            }
        });
    });
    ymaps.ready(init);
    function init () {
        var multiRoute = new ymaps.multiRouter.MultiRoute({
            referencePoints: [
                "Москва, Волгоградский проспект, дом 96/2",
                "Москва, Успенский пер. 7"
            ],
            params: {
                routingMode: 'masstransit'
            }
        });


        // Создаем карту с добавленной на нее кнопкой.
         myMap = new ymaps.Map('map', {
            center: [55.754087, 37.620249],
            zoom: 9
        }, {
            buttonMaxWidth: 300
        });

        // Добавляем мультимаршрут на карту.
        myCollection = new ymaps.GeoObjectCollection();
        myMap.geoObjects.add(multiRoute);
    }


});




//Загрузка заказа в блок деталей
function setDetail(order_id){
    $.ajax({
        method: "POST",
        url: "/engine/classes/Ajax.php",
        data: {
            action: 'get_order',
            data: {
                id: order_id
            }
        },
        success: function(response){
            var that=this;
            $('.details').removeClass('hidden');
            $('#newOrder').addClass('hidden');
            var resp = JSON.parse(response);
            var data= JSON.parse(resp.data);
            var prod_err=false;
            var error;
            try{
                data.contents=JSON.parse(data.contents);
            }
            catch(e){
                error=e;
                prod_err=true;
            }
            Order=data;

            $('#order_id').text(data.id);
            getLogs();
            var orders="";
            var order_sum=0;
            var order_count=0;
            var multiRoute = new ymaps.multiRouter.MultiRoute({
                referencePoints: [
                    "Москва, Волгоградский проспект, дом 96/2",
                    data.client.address
                ],
                params: {
                    routingMode: 'masstransit'
                }
            });
            myMap.geoObjects.removeAll();
            myMap.geoObjects.add(multiRoute);

            if(!prod_err) {
                $("#order_items").html(toOrderProductsView(Order.contents));
            }else{
                $("#order_items").html("<tr class='alert-tr'><td><strong>Ошибка! Обратитесь к программисту.</strong><br>"+error+"</td></tr>");
            }
            $('.comment').popover({
                trigger :'hover'
            });
            getCouriers();
            $('[role="client_name"]').val(data.client.name);
            $('[role="state"]').attr("class","");
            $('[role="state"]').addClass('label label-'+data.state.class).text(data.state.name);
            $('[role="client_email"]').val(data.client.email);
            $('[role="client_pfone"]').val(data.client.phone);
            $('[role="client_adres"]').val(data.client.address);
            $('[role="client_сomment"]').val(data.comment);
            $('[role="client_sdacha"]').val(Order.sdacha);

            $('[role="order_time_travel"]').val(Order.time_travel);
            $('[role="order_time_to"]').val(Order.time_to);
            $('[role="order_time_cook"]').val(Order.time_cook);

            if(data.state_id==4 || data.state_id==5){
                $('[role="client_courier"]').attr('disabled', 'disabled');
            }
            else{
                $('[role="client_courier"]').removeAttr('disabled');
            }
            var states=$('[state_id]');
            for(var i=0;i<states.length;i++){
                $(states[i]).removeClass('active_cat');
                if($(states[i]).attr('state_id')==data.state_id)
                {
                    $(states[i]).addClass('active_cat');
                }
            }

        }
    });
}
function getProductById(id){
    for(var i=0;i<Products.length;i++){
        if(Products[i].id==id){
            return Products[i];
        }
    }
    return null;
}
function  getLogs(){
    $.ajax({
        method: "POST",
        url: "/engine/classes/Ajax.php",
        data: {
            action: 'getLogs',
            data: Order.id
        },
        success: function(response){
            var logs= JSON.parse(response);
            logs=JSON.parse(logs.data);
            Logs = logs;
        }
    });

}
function toLogsView(logs){
    var log_html="";
    for(var i=0;i<logs.length;i++){
        log_html+="<tr><td>"+logs[i].order_id+"</td><td>"+logs[i].comment+"</td><td>"+logs[i].date+"</td></tr>";
    }
    return log_html;
}
function getProducts(){
    $.ajax({
        method: "POST",
        url: "/engine/classes/Ajax.php",
        data: {
            action: 'get_products'
        },
        success: function(response){
            var products = JSON.parse(response);
            products = JSON.parse(products.data);
            Products = products;
        }
    });
}
function toProductsView(products){
    var products_html="";
    prod=products;
    var products_html="<table class='table table-bordered'>";
    for(var i=0;i<prod.length;i++) {
        if(prod[i].disabled==0) {
            products_html += "<tr class='item'><td><img src='/img/products/" + prod[i].img + "' class='img_admin'></td><td>" + prod[i].name + "</td><td><strong>" + prod[i].price + "</strong></td><td><i prod_id='"+prod[i].id+"' class='fa pointer redf fa-plus add_prod_cart' aria-hidden='true'></i></td></tr>";
        }
    }
    products_html+="</table>";
    return products_html;
}

function toOrderProductsView(order_products){
    var products = order_products;
    var products_html="";
    var order_count=0;

        for (var i = 0; i < products.length; i++) {
            order_count += products[i].count;
            if(products[i].comment===undefined || products[i].comment==null){
                products[i].comment="";
            }
            products_html += "<tr><td>" + products[i].name + "</td><td>" + products[i].count + "x</td><td><a data-prod-i='"+i+"' tabindex='0' class='btn btn-default comment' role='button' data-toggle='popover' data-trigger='focus' title='"+products[i].name+"' data-content='"+products[i].comment+"'><i class='fa fa-commenting-o' aria-hidden='true'></i></a></td><td><i prod_id='" + products[i].id + "' class='fa pointer fa-plus add_prod_cart' aria-hidden='true'></i></td><td><i prod_id='" + products[i].id + "' class='fa pointer fa-minus del_prod_cart' aria-hidden='true'></i></td><td>" + products[i].price + " <i class='fa fa-rub' aria-hidden='true'></i></td><td>"+products[i].comment+"</td></tr>";

        }

    products_html+="<tr class='hidden add_prod_input'><td><input id='autoinput' type='text' class='update_input'></td></tr><tr><td><strong>Сумма</strong></td><td><strong>"+order_count+"x</strong></td><td></td><td></td><td></td><td><strong>"+getOrderCash(products)+"</strong> <i class='fa fa-rub' aria-hidden='true'></i></td></tr>";

    return products_html;
}
function getOrderCash(order_products){
    var products = order_products;
    var order_sum = 0;
    for(var i=0;i<products.length;i++) {
        order_sum += products[i].count * products[i].price;
    }
    return order_sum;
}

function verifyOrderToNew(order){
    if(Old_Orders.length!=0) {

        var isNew=false;
        //var orders=filtrOrders(Current_state,Orders);
        for (var i = 0; i < Old_Orders.length; i++) {
            if (order.id == Old_Orders[i].id) {
                if(Old_Orders[i].new==1 || Old_Orders[i].state_id!=order.state_id){
                    order.new=1;
                    if((Current_state==order.state_id || order.state_id==1) && order.new==1){
                        $('#chatAudio')[0].play();
                    }
                }
                else{
                    order.new=0;
                }
                return order;
            }
            else{
                order.new=1;
                if(order.state_id==1 && order.new==1){
                    $('#chatAudio')[0].play();
                }
            }
        }
    }
    return order;
}

function toOrderView(orders){
    var order;
    var classes="";
    var danger="";
    var order_html="";
    for(var i=0;i<orders.length;i++){
        order = orders[i];
        if(order.state_id==3 && order.courier_id==0){
            danger="<br><i class='fa redf fa-exclamation-triangle' aria-hidden='true'></i><span><strong class='redf'>Курьер отсутствует!</strong></span>";
        } else{
            danger="";
        }
            if (order.new == 1) {
                classes = "new_order"
            }
            else {
                classes = "";
            }
        order_html+="<tr class='item "+classes+" det left-border-"+order.state.class+"'  data-id='"+order.id+"'><td><p><strong>№ Заказа: </strong>"+order.id+" <span role='state_min' style='color: #fff;' class='label label-"+order.state.class+"'>"+order.state.name+"</span>"+danger+"<br><strong>Стоимость:</strong> "+order.cost+"</p> </td> <td class='text-right'><p>"+order.date_created+"<br><strong>"+order.client.phone+"</strong></p></td> </tr>";
    }
    return order_html;
}

//Функция вывода всех заказов
function filtrOrders(state_id,orders){
    var forders=[];
    if(state_id==0){
        forders=orders;
    }
    else {
        for (var i = 0; i < orders.length; i++) {
            if (orders[i].state_id == state_id) {
                forders.push(orders[i]);
            }
        }
    }
    //if(order==undefined){setDetail(forders[i].id);}
    //$('#order_table').html(toOrderView(forders));
    return forders;
}
function getOrders(days,verifyNew){
    if (verifyNew===undefined){
        verifyNew=true;
    }
    if(days===undefined){
        days=1;
    }
    $.ajax({
        method: "POST",
        url: "/engine/classes/Ajax.php",
        data: {
            action: 'get_orders',
            days: days
        },
        success: function(response){
            var orders = JSON.parse(response);
            orders = JSON.parse(orders.data);
            Old_Orders = Orders;
            for(var i=0;i<orders.length;i++){
                orders[i].new=0;
                if(verifyNew) {
                    orders[i] = verifyOrderToNew(orders[i]);
                }
                try {
                    orders[i].contents = JSON.parse(orders[i].contents);
                    orders[i].cost=getOrderCash(orders[i].contents);
                }
                catch (err){

                    if(!Array.isArray(orders[i].contents)){
                        orders[i].contents=[];
                    }
                }
            }
            Orders = orders;
            if(Current_state!=888) {
                $('#order_table').html(toOrderView(filtrOrders(Current_state, Orders)));
            }
            setCategoryNums();

        }
        }

    );

}

function setCategoryNums(){
    var categories=$(".order_counter");
    for(var i=0;i<categories.length;i++){
        $(categories[i]).text(getCategoryCounts(Orders,$(categories[i]).attr('value')));
        if($(categories[i]).attr('value')==0){
            $(categories[i]).text(Orders.length);
        }
    }
}

function getCategoryCounts(orders,category_id){
    var counter=0;
    for (var i=0;i<orders.length;i++){
        if(orders[i].state_id==category_id)
        {
            counter++;
        }
    }
    return counter;
}

function printCouriers(couriers){
    var str="<option value='0'>Нет курьера</option>";
    var select="";
    for(var i=0;i<couriers.length;i++){
        if(couriers[i].id==Order.courier_id){select="selected";}else{select="";}
        if(couriers[i].free==1 || couriers[i].id==Order.courier_id){
            str+="<option "+select+" value='"+couriers[i].id+"'>"+couriers[i].name+"  "+couriers[i].last_name+"</option>";
        }
    }
    $('[role="client_courier"]').html(str);
}

//Возвращает массив курьеров
function getCouriers(){
    $.ajax({
        method: "POST",
        url: "/engine/classes/Ajax.php",
        data: {
            action: 'get_couriers'
        },
        success: function(response){
            var couriers = JSON.parse(response);
            couriers = JSON.parse(couriers.data);
            printCouriers(couriers);
        }
    });
}