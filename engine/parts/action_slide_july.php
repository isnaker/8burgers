<div class="fullscreen-slide" style="background-image: url('/img/slides/1.jpg')">
    <div class="july_promo_text">
        <p class="h3 nomargin wow zoomIn" style="position: relative; top: 0.7em;">
            <strong>ТОЛЬКО ДО КОНЦА ИЮЛЯ</strong>
        </p>
        <p style="font-size: 4em;" class="nomargin wow zoomIn" data-wow-delay="800ms">
            <strong class="color-yellow font-opensans">ДАРИМ</strong>
        </p>
        <p style="font-size: 2.6em" class="nomargin  wow fadeIn font-opensans" data-wow-delay="1800ms">
            <strong class="color-yellow font-opensans"  style="font-size: 2em;">300</strong> РУБЛЕЙ
        </p>

        <p style="position: relative; bottom: 0.6em" class="mt-none h2 wow fadeIn font-opensans" data-wow-delay="1900ms">
            НА КАЖДЫЙ ЗАКАЗ</p>

        <p  class="h3 text-right wow fadeIn" data-wow-delay="2500ms" style="position: relative; bottom: 1em;">
            Условия акции смотрите в разделе <a class="color-yellow" href="/actions/">Акции</a>
        </p>
    </div>
</div>