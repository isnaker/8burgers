<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GEEKBURGER - Доставка бургеров ручной работы по Москве</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet">
    <link href="/css/slick-theme.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/jquery.fancybox.css" rel="stylesheet">
    <link href="/css/responsive.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/jquery.suggestions/16.5.2/css/suggestions.css" type="text/css" rel="stylesheet" />

    <link href="/css/geekburger.css" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/img/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/img/favicons/manifest.json">
    <link rel="mask-icon" href="/img/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/img/favicons/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<header id="start">
    <div class="setka">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-3">
                    <div class="logo-container">
                    <span id="logotype">
                        <a href="/" class="font-opensans">
                            GEEKBURGER
                        </a>
                    </span>
                    </div>
                </div>
                <div class="col-md-3 hidden-xs hidden-sm">
                    <p class="font-opensans mt-xlg"> Доставка вкуснейших бургеров в Москве за 60 минут. </p>
                </div>
                <div class="col-md-4 col-xs-6">
                    <div class="text-center">
                        <p id="contacts" class="h3 mb-none">
                            <strong class="font-opensans">8 (495) 532-62-29</strong></p>
                        <small class="font-opensans color-yellow">Прием заказов по телефону.</small>
                    </div>
                </div>
                <div class="col-md-1 col-xs-3 col-md-offset-1">
                    <a href="/cart" class="cart-button">
                        <small>Корзина</small>
                        <i class="fa fa-shopping-cart"></i> <span data-value="cart-count"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>


<?php include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/menu.php"); ?>
<?php  if (isset($headSlider)) {print $headSlider; } ?>

<div id="content">
    <?php if (isset($content)) {print $content; } ?>
    <?php if (isset($includable)){
        foreach($includable as $item)
        {
            include_once($_SERVER['DOCUMENT_ROOT'].$item);
        }
    } ?>
</div>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <img src="/img/logo.png" class="img-responsive">
            </div>
            <div class="col-md-3">
                <div class="logo-container">
                    <p class="h2 company-title">
                        <strong class="font-opensans">8 burgers</strong></p>
                    <p>Адрес: г. Москва, Волгоградский проспект, дом 96/2</p>
                    <p>ООО "Бургер Хаус"</p>
                </div>
                <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter38077490 = new Ya.Metrika({ id:38077490, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/38077490" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
            </div>
            <div class="col-md-3">
                <p class="h3">Мы в социальных сетях</p>
                <p class="h4">
                    <a href="http://vk.com/8burgers" target="_blank" class="font-opensans">
                        <i class="fa fa-vk"></i> /8burgers</a>
                </p>
                <p class="h4">
                    <a href="https://www.instagram.com/8_burgers" target="_blank" class="font-opensans">
                        <i class="fa fa-instagram"></i> /8_burgers</a>
                </p>
            </div>
            <div class="col-md-4">
                <div class="text-right">
                    <p class="h5 font-opensans mb-none">Наш телефон:</p>
                    <p class="h3 mt-none"><strong class="font-opensans color-yellow">8 (495) 532-62-29</strong></p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 text-right">
                <a href="http://www.snapix.ru" target="_blank">Snapix - Разработка и продвижение сайтов</a>
            </div>
        </div>
    </div>
</footer>


<div class="modal fade" id="callbackModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p class="h5 modal-title"><strong>Заказать обратный звонок</strong></p>
            </div>
            <div class="modal-body">
                <p>Наши менеджеры свяжутся с Вами в самое ближайшее время!</p>
                <div class="form-group">
                    <label>Ваш телефон:</label>
                    <input
                        data-label="телефон"
                        data-value="phone"
                        class="form-control text-center input required"
                        type="tel" placeholder="Введите телефон">
                </div>
            </div>
            <div class="modal-footer">
                <button data-heading="Заявка на обратный звонок" data-parent="#callbackModal" type="button" class="btn btn-danger center-block sender"><strong>Отправить заявку</strong></button>

                <div class="alert hidden responder text-center"></div>
                <div style="margin-top: 1em" class="text-center"><small><i class="fa fa-lock"></i> Ваши данные в безопасности и не передаются третьим лицам.</small></div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/jquery.min.js"></script>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.maskedinput.min.js"></script>
<script src="/js/slick.min.js"></script>

<script src="/js/wow.js"></script>
<script src="/js/jquery.easing.min.js"></script>
<script src="/js/fancybox/jquery.fancybox.pack.js"></script>
<script src="/js/scripts.js"></script>
<script src="/js/sliders.js"></script>
<script src="/js/forms.js"></script>
<script src="/js/cart.js"></script>

</body>
</html>