<div class="slick-slider">
    <div class="fullscreen-slide wow fadeIn" style="background-image: url('/img/slides/summer_burgers.jpg')"></div>

    <div class="fullscreen-slide" id="promo">
        <div class="promo__content">
            <div class="promo__heading wow fadeIn"><span class="wow fadeIn"  data-wow-delay="300ms">geek</span>burgers <i class="fa fa-star-o"></i></div>
            <p class="promo__subheading font-opensans wow slideIn"  data-wow-delay="400ms">выбери свой героический бургер!  </p>
            <a class="btn btn-danger btn-lg font-opensans text-uppercase mt-lg hidden" href="/geekburgers">выбрать сторону</a>

            <p class="h3 text-center coming_soon font-opensans text-uppercase mt-xlg">уже скоро</p>
        </div>
    </div>
</div>
