<nav id="stick_navigation" class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand font-opensans logo" href="/"><span>8burgers</span></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="mobile-menu">
            <ul class="nav navbar-nav" >
                <?php
                $routes = App::routes();
                $isactive = "";

                foreach ($routes as $route){
                    if (isset($current_page)){
                        $route['page_num'] == $current_page ?  $isactive = "current" : $isactive = "";
                    }

                    ?>
                    <li><a title="<?php print $route['name']; ?>"
                           class="page-scroll <?php print $isactive; ?>"
                           href="<?php print $route['url']; ?>">
                            <?php print $route['content']; ?>
                        </a></li>
                <?php  } ?>
            </ul>
            <ul class="nav navbar-nav navbar-right visible-fixedmenu">
                <li><p class="h2 phone text-center"><strong class=" font-opensans">8 (495) 532-62-29</strong></p> </li>
                <li><a href="/cart/" class="cart-button">
                        <small>Корзина</small>
                        <i class="fa fa-shopping-cart"></i> <span data-value="cart-count"></span></a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div id="promo-content">
    <section id="menu" class="">
        <nav id="navigation" class="navbar navbar-default ">

            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-right navbar-nav text-center">
                        <?php

                             foreach ($routes as $route){

                                 if (isset($current_page)){
                                        $route['page_num'] == $current_page ?  $isactive = "current" : $isactive = "";
                                 }

                                 ?>
                                    <li><a title="<?php print $route['name']; ?>"
                                           class="page-scroll <?php print $isactive; ?>"
                                           href="<?php print $route['url']; ?>">
                                            <?php print $route['content']; ?>
                                    </a></li>
                        <?php  } ?>
                    </ul>
                </div>
            </div>
        </nav>
    </section>
</div>