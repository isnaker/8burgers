<div id="page-product">
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li><a href="/"><i class="fa fa-home"></i> </a></li>
                <li><a href="/menu/">Меню</a></li>
                <li><a href="/menu/<?php print $product->category->seo_url; ?>"><?php print $product->category->name; ?></a></li>
                <li class="active"><?php print $product->name ?></li>
            </ol>
            </div>
        </div>

            <div class="product-card row">

                <div class="col-md-6 image text-center">
                    <div class="responder alert alert-success text-center ">
                        <i class="fa fa-check-circle fa-3x"></i>
                        <p class="font-opensans"> Добавлен в корзину!</p>
                    </div>
                    <?php if (count($product->images) > 0){ ?>
                        <img src="/img/products/<?php echo $product->images[0]->image_url; ?>"
                             class="product-image img-responsive wow zoomIn">
                    <?php } ?>

                    <?php if ($product->category->id == 1){ ?>
                        <img width="400" src="/img/free2.png" class="mt-xlg img-responsive product-banner center-block wow flipInX" data-wow-delay="0.5s">
                    <?php } ?>
                    <?php if ($product->category->id == 9){ ?>
                        <img width="400" src="/img/sous.png" class="mt-xlg img-responsive product-banner center-block wow flipInX" data-wow-delay="0.5s">
                    <?php } ?>

                </div>
                 <div class="col-md-6 info">

                     <a href="/menu/" class="back-link"><i class="fa fa-reply"></i> Назад</a>
                     <p class="h1 font-opensans mb-none"><?php print $product->name ?></p>
                     <span star_count="<?php print $product->rating; ?>" class="stars"></span>
                     <p class="description font-opensans mt-md"><?php print $product->description ?></p>

                     <?php if (count($product->ingredients) > 0) { ?>
                         <p class="h5 font-opensans mt-xlg">Состав:</p>
                         <div class="ingredients-list">
                             <?php foreach($product->ingredients as $item) { ?>
                                 <span class="ingredient-item"><?php print $item->name ?></span>
                             <?php } ?>
                         </div>
                     <?php } ?>


                     <p class="h2">
                         <span class="price font-opensans"><?php print $product->price  ?></span>  <i class="fa fa-rouble"></i>
                         <small class="weight"> <span class="font-opensans"><?php print $product->weight  ?></span>  гр. <i class="fa fa-balance-scale"></i> </small>
                     </p>
                     <a class="btn btn-lg btn-danger addtocart font-opensans" data-count="1" data-id="<?php print $product->id; ?>">
                        <span><i class="fa fa-shopping-basket"></i></span> В корзину</a>

                     <?php

                        $cart = Cart::getJSON();
                        if (count($cart) > 0){
                            foreach ($cart as $item){
                                if ($item['id'] == $product->id){ ?>
                                    <p>Уже в корзине: <?php print $item['count']; ?></p>
                                <?php  }
                            }
                        }
                     ?>



                </div>
            </div>

        <div class="row">
            <div class="col-md-12">
                <p class="h3 font-opensans"> Другие продукты</p>

                <div class="products mt-lg">
                    <?php if (isset($products) && count($products) > 0) { ?>
                        <?php foreach ($products as $item){

                            if (count($item->images) == 0) { // if array is empty

                                $img = new stdClass();
                                $img->image_url = "burger2.png";
                                array_push($item->images, $img);
                            };
                            ?>
                            <div class="col-md-3">
                                <div class="item" data-id="<?php print $item->id; ?>">
                                    <div class="responder">
                                        Добавлено в корзину!
                                    </div>
                                    <a class="h2 text-center text-uppercase heading" href="/menu/product/<?php print $item->seo_url ?>">
                                        <?php print $item->name; ?>
                                    </a>

                                    <div class="rating text-center">
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>

                                    <img src="/img/products/<?php print $item->images[0]->image_url; ?>" class="img-responsive center-block product-image">
                                    <div class="product-info">
                                        <p class="h2">
                                            <span class="price font-opensans"><?php print $item->price  ?></span>  <i class="fa fa-rouble"></i>
                                            <small class="weight"> <span class="font-opensans"><?php print $item->weight  ?></span>  гр. <i class="fa fa-balance-scale"></i> </small>
                                        </p>

                                        <div class="controls clearfix">
                                            <a class="btn btn-lg btn-danger addtocart font-opensans" data-count="1" data-id="<?php print $item->id; ?>">
                                                <span><i class="fa fa-shopping-basket"></i></span> В корзину</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php  } ?>
                    <?php }  else { ?>

                        <div class="text-center"><p class="h4 color-white">Нет продуктов.</p> </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>