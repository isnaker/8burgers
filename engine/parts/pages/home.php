<section class="table-bg" id="home-products">
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <p class="h1 font-opensans text-center text-uppercase mb-none">бургеры <span class="color-yellow font-opensans"> ручной работы</span> специально для вас.</p>
            <p class="h4 text-center mt-md mb-xlg font-opensans">Из мраморной говядины. Со свежими овощами. Без консервантов.</p>
            <div class="products mt-lg">
                <?php if (isset($products) && count($products) > 0) { ?>
                    <?php foreach ($products as $item){

                        if (count($item->images) == 0) { // if array is empty

                            $img = new stdClass();
                            $img->image_url = "burger2.png";
                            array_push($item->images, $img);
                        };
                        ?>
                        <div class="col-md-3">
                            <div class="item" data-id="<?php print $item->id; ?>">
                                <div class="responder">
                                    Добавлено в корзину!
                                </div>
                                <a class="h2 text-center text-uppercase heading" href="/menu/product/<?php print $item->seo_url ?>">
                                    <?php print $item->name; ?>
                                </a>

                                <div class="rating text-center">
                                    <div star_count="<?php print $item->rating; ?>" class="stars"></div>
                                </div>

                                <img src="/img/products/<?php print $item->images[0]->image_url; ?>" class="img-responsive center-block product-image">
                                <div class="product-info">
                                    <p class="h2">
                                        <span class="price font-opensans"><?php print $item->price  ?></span>  <i class="fa fa-rouble"></i>
                                        <small class="weight"> <span class="font-opensans"><?php print $item->weight  ?></span>  гр. <i class="fa fa-balance-scale"></i> </small>
                                    </p>

                                    <div class="controls clearfix">
                                        <a class="btn btn-lg btn-danger addtocart font-opensans" data-count="1" data-id="<?php print $item->id; ?>">
                                            <span><i class="fa fa-shopping-basket"></i></span> В корзину</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php  } ?>
                <?php }  else { ?>

                    <div class="text-center"><p class="h4 color-white">Нет продуктов.</p> </div>
                <?php } ?>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-md-12 text-center mb-xlg">
                <a href="/menu/burgers" class="btn btn-danger btn-lg"><strong>Всё меню >></strong></a>
            </div>
        </div>
</div>
</section>

<section id="why">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="h1 text-uppercase heading font-opensans">почему выбирают нас?</p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <i class="fa fa-star fa-3x"></i>
                <i class="fa fa-star fa-3x"></i>
                <i class="fa fa-star fa-3x"></i>
                <i class="fa fa-star fa-3x"></i>
                <i class="fa fa-star fa-3x"></i>
                <p class="h3 font-opensans color-yellow"> Высочайшее качество продуктов</p>
            </div>
            <div class="col-md-4">
                <p class="h1 font-opensans mt-none"> <i class="fa fa-clock-o fa-1x"></i> 60 мин.</p>

                <p class="h3 font-opensans color-yellow">Доставка за 60 минут</p>
                <p class="h5 text-center font-opensans">или быстрее!</p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                <p class="h1 font-opensans mt-lg"> <i class="fa fa-trophy fa-2x"></i></p>
                <p class="h2 font-opensans color-yellow">Вкуснее, чем в ресторанах,</p>
                <p class="h5 text-center font-opensans">потому что от сердца и с душой!</p>
            </div>
            <div class="col-md-4">
                <p class="h1 font-opensans mt-lg"> <i class="fa fa-clock-o fa-2x"></i></p>
                <p class="h2 font-opensans color-yellow">Помним о своих клиентах</p>
                <p class="h5 text-center font-opensans">и даем скидки и вкусные бонусы!</p>
            </div>
        </div>
    </div>
</section>
