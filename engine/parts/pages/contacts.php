<div id="page-contacts">
    <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="h1 font-opensans text-center mb-xlg">Контакты</p>
                </div>
            </div>

        <div class="col-md-10 col-md-offset-1">
            <div class="row text-center">
                <div class="col-md-4">
                    <p class="h4 font-opensans">Адрес:</p>
                    <p class="h3">г. Москва, Волгоградский проспект, дом 96/2</p>
                </div>

                <div class="col-md-4">
                    <p class="h4 font-opensans">Телефон:</p>
                    <p class="h3">8 (495) 532-62-29</p>
                </div>

                <div class="col-md-4">
                    <p class="h4 font-opensans">E-mail:</p>
                    <p class="h3">info@8burgers.ru</p>
                </div>

                <div id="map"></div>
            </div>
        </div>
    </div>
</div>
