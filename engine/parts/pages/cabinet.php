<style>

    .aboutme_address{
        background: #29201b;
        min-height: 43.5em;
    }

    #cabinet .main_container{
        padding: 0 3.5em 0 6.5em;
    }
    #cabinet .title{
        color: #e4ded2;
        padding-top: 0.3em;
        padding-left: 0.5em;
        font-size: 1.8em;
        letter-spacing: 0.03em;
    }

    #cabinet .aboutme_address .list-group{
        margin: -1em 1em;
        padding: 0;
    }

    #cabinet .panel,
    #cabinet .panel-default,
    #cabinet .panel-body,
    #cabinet .list-group,
    #cabinet .list-group-item,
    #cabinet .list-group .active
    {
        background: none;
        border: none;
        color: #e4ded2;

    }

    #cabinet .aboutme_address .panel{
margin-bottom: 4em;
    }

    #cabinet .item{
        background: #b2aead;
        min-height: 6em;
        margin: 2px 6px 0 -11px;
        padding: 0.2em 1em;
    }


    #cabinet .item-panel{
        background: #e3e2e0;
        margin: -2px -13px;
        padding: 1px 15px;
    }

    #cabinet .item-panel > i{
        display: block;
        font-size: 1.7em;
        margin: 0.5em 0;
        color: #000;
    }

    #cabinet .item-panel > i:hover{
        cursor: pointer;
        color: #2b542c;
    }


    #cabinet input,
    #cabinet div,
    #cabinet a,
    #cabinet button{
        border: none;
        border-radius: 0px;col-md-4
        color: #000;
        font-weight: bold;
        text-shadow: none;
    }

    #cabinet .change-pass{
        color: #fff;
        margin-bottom: 1em;
    }

    #cabinet .form-auth p{
        margin: 0;
    }
    #cabinet .form-auth{
        margin: -4em -2em;
    }
    #cabinet .img-profile{
        width: 85%;
        margin-top: -4em;
    }
    #cabinet input{
        background: #b2aead;
    }

    #cabinet .discount{
        background: url('../../../img/cabinet/discount.png');
        background-repeat: no-repeat;
        background-size: 87%;
        height: 7em;
        padding: 1em 0.7em 0 0em;
        margin: 1em 0;
    }

    #cabinet .discount> span{
        display: block;
        color: red;
        font-weight: bold;
        text-align: center;
    }
    #cabinet .discount> span.d-val{
        font-size: 2em;
        margin: -10px 0;
        text-shadow: 1px 1px 0 #000;
    }

    #cabinet .zakazi{
        background: rgba(59, 51, 48, 1);
        position: relative;
        height: 43.5em;
    }

    #cabinet .zakazi p{
        display: flex;
        margin: 0;
    }

    #cabinet .zakazi p.active{
        padding: 1.3em 0 0.8em 0;
    }

    #cabinet .zakazi .list-group{
        height: 100%;
        margin-bottom: -5em;
    }

    #cabinet .zakazi .item span{
        display: inline-block;
        vertical-align: top;
    }

    #cabinet span.date,
    #cabinet span.zakaz,
    #cabinet span.count{
        color: #000;
    }

    #cabinet span.date{
        font-size: 1em;
        font-weight: initial;
        width: 7em;
    }
    #cabinet span.zakaz
    {
        width: 13em;
        font-size: 0.7em;
    }
    #cabinet span.count{
        font-size: 1.5em;
        width: 7em;
    }
    #cabinet span.look{
        color: #D00606;
        width: 12em;
        text-align: right;
        margin-top: 0.7em;
    }

    #cabinet span.look:hover{
        text-decoration: underline;
        cursor: pointer;
    }

    #cabinet .aboutme_address .item
    {
        color: #000;

    }

    #cabinet .exit{
        background: none;
        font-size: 1.5em;
        font-weight: initial;
        color: #e4ded2;
        margin-top: 1em;
    }

    }

    .btn-add-address{
        padding: 0.3em 4em;
        margin-left: 0.5em;
    }
    .title-myaddresses{font-size: 1.85em;}
    .list-address{
        margin: 0 2em;
    }
    #cabinet .list-address .text{
        font-weight: normal;
        color: #000;
        margin: 0;
        padding: 0;
        display: inline-block;
    }
    #cabinet .zakaz-path{
        margin: 0 -15px;
        min-height: 4.5em;
        padding: 0.2em;
    }

    .save-changes,
    .btn-add-address{
        color: #000;
    }
    .list-address-val{
        font-weight: normal;
    }

    @media all and  (max-width: 1200px) {
        #cabinet .discount{
            padding: 0.5em 0.5em 0 0;
        }
        #cabinet .form-auth{
            width: 20em;
        }
        #cabinet .discount> span{
            font-size: 0.7em;
            margin: 0.5em 0;
        }
        #cabinet .discount> span.d-val{
            font-size: 1.7em;
        }
        #cabinet span.count{
            width: 8em;
        }

    }

    @media all and  (max-width: 992px) {
        #cabinet .img-profile{
            width: 40%;
            margin-top: -4em;
            margin-bottom: 4em;
        }
        #cabinet .zakazi p.active{
            margin: 0.6em 0;
        }
        #cabinet .aboutme_address .item{
            padding-bottom: 1em;
        }
        .list-address{
            padding-bottom: 1em;
        }
        #cabinet .discount> span.d-val{
            font-size: 1.5em;
        }
        #cabinet .discount{
            padding: 0.5em 0.5em 0 0;

        }
        #cabinet .zakazi .item span{
            width: 50%;
        }
    }

    @media all and  (max-width: 768px) {

        #cabinet .title{
            padding: 0.5em 1.5em 1em 0;
        }

        #cabinet .discount{
            margin-top: 2em;
            background-size: 65%;
            padding: 0.8em 3.5em 0 0.7em;
        }

</style>

<section id="cabinet">
    <div class="container main_container">
        <div class="row">
            <div class="col-md-9 aboutme_address">
                        <div class="col-md-10 col-sm-10 col-xs-9">
                            <p class="h2 title">Личный кабинет</p>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-3">
                            <p class="discount text-danger">
                                <span>скидка</span>
                                <span class="d-val">100</span>
                                <span>%</span>
                            </p>
                        </div>



                <!--инф о клиенте-->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-4">
                            <img src="../../../img/cabinet/foto-default.png" alt="foto profile" class="img-profile">
                            <span class="glyphicon glyphicon-pencil"></span>
                            <span class="glyphicon glyphicon-remove"></span>
                        </div>
                        <div class="col-md-4 form-auth">
                            <p>Имя</p>
                            <input class="form-control" type="text" placeholder="name">
                            <p>Фамилия</p>
                            <input class="form-control" type="text" placeholder="surname">
                            <p>Телефон</p>
                            <input class="form-control" type="text" placeholder="tel">
                            <a href="" class="change-pass pull-right">сменить пароль >></a>
                            <button class="btn btn-danger btn-block save-changes">Сохранить изменения</button>
                        </div>
                    </div>
                </div>
                <hr class="line_bottom">

                <!--адреса-->
                <div class="list-group address">
                    <p class="list-group-item active h3 title-myaddresses">
                        Мои адреса доставки
                        <button class="btn btn-danger btn-sm btn-add-address">Добавить адрес</button>
                    </p>

                    <div class="list-address">


                            <div class="list-group-item item list-address-val">
                                <p class="text">address №1</p>
                                <div class="pull-right item-panel">
                                    <i class="fa fa-edit" title="изменить"></i>
                                    <i class="fa fa-trash" title="удалить"></i>
                                </div>
                            </div>


                    </div>

                </div>
            </div>

            <!--заказы-->
            <div class="col-md-3 zakazi">
                <div class="list-group">
                    <p class="list-group-item active h3">Мои заказы</p>



                        <div class="list-group-item item zakaz-path">
                            <p>
                                <span class="date">10.06.2016</span>
                                <span class="zakaz">бургеры из хлеба и мяса, напитки из пепси и фанты, пиво и сигареты</span>
                            </p>
                            <p>
                                <span class="count">659 руб.</span>
                                <span class="look">Посмотреть</span>
                            </p>

                        </div>

                    
                </div>
                <button class="btn btn-danger btn-block exit">Выйти</button>
            </div>
        </div>
    </div>
</section>