<div id="page-cart">



<?php if (count($app->cart->products) > 0) {
    $total = 0;
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">

                <div id="cart-contents">
                    <p class="h3 color-white"><strong>Состав заказа:</strong></p>

                    <div data-value="cart-contents"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="cart-order-form">
                    <div class="row">
                        <p class="h2  text-center">
                            <strong class="color-white">Сумма заказа:</strong><br>
                            <strong class="font-opensans price  font-opensans" ><span data-value="data-cart-total" class=" font-opensans"></span></strong>
                            <i class="fa fa-rouble"></i>
                        </p>
                        <div id="cart-too-few-alert" class="alert alert-danger font-opensans hidden">
                            Минимальная сумма заказа - <span class="color-black font-opensans"> 600</span> <i class="color-black fa fa-rouble"></i>
                        </div>
                    </div>

                    <div id="cart-controls" class="hidden">
                        <div class="row">
                            <div class="col-md-12">
                                <input class="form-control" type="tel" data-value="phone" placeholder="Ваш телефон">
                            </div>
                            <div class="col-md-12">
                                <textarea id="address" class="form-control mb-none" data-value="address" placeholder="Адрес доставки"></textarea>
                            </div>
                            <div class="col-md-6"><p class="font-opensans mt-md"> Квартира/офис: </p></div>
                            <div class="col-md-6">
                               <input class="form-control" data-value="apartment" placeholder="кв/офис">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p id="add_comment_switcher" class="font-opensans text-center"> <i class="fa fa-plus"></i> Добавить комментарий</p>
                                <textarea class="form-control hidden" data-value="comment" placeholder="Комментарий"></textarea>
                            </div>
                        </div>

                        <button id="create_order" class="btn btn-danger btn-lg btn-block"><strong class="text-uppercase  font-opensans">оформить заказ</strong></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else { ?>
    <div class="container">
        <div class="row text-center">
            <div class="mt-xlg">
                <i class="fa fa-shopping-basket fa-5x"></i>
                <p class="h2 font-opensans">Ваша корзина пуста.</p>
                <a href="/menu/" class="btn btn-danger btn-lg mt-xlg">Перейти в меню</a>
            </div>
        </div>
    </div>
<?php } ?>

</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--[if lt IE 10]>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<![endif]-->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.suggestions/16.5.2/js/jquery.suggestions.min.js"></script>
<script type="text/javascript">
    $("#address").suggestions({
        serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
        token: "bc4bafa3dda85fe0b8c7a8c84d513d314528cf8a",
        type: "ADDRESS",
        count: 5,
        restrict_value: true,

        constraints: {
            label: "",
          locations: { region: "Москва"}
        },
        onSelect: function(suggestion) {
            console.log(suggestion);
        }
    });
</script>