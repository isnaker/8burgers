<section id="page-actions">
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <p class="h1 font-opensans text-center mb-lg mt-lg">С 1 июля по 31 июля 2016 года в нашей компании действует акция:</p>

            <div class="row text-center">

            <div class="col-md-4">
                <i class="fa fa-file fa-5x"></i>
                <p class="h4 mt-xlg">Для получения скидки – Вам необходимо предъявить курьеру
                    <strong class="font-opensans">Купон номиналом 300 рублей.</strong> </p>
            </div>

            <div class="col-md-4">
                <i class="fa fa-smile-o fa-5x"></i>
                <p class="h4 mt-xlg">Купоном можно оплатить сумму в 300 рублей,
                    <strong> не входящую в
                        минимальную стоимость (600 рублей)</strong></p>
            </div>

            <div class="col-md-4">
                <i class="fa fa-gift fa-5x"></i>
                <p class="h4 mt-xlg">Один человек может использовать купон неограниченное
                    количество раз,
                    но не более одного купона на один заказ.</p>
            </div>
            </div>


            <hr class="mb-none mt-xlg">
            <p class="mb-xlg mt-none">Купоны и акции не складываются.
            Акция распространяется на все меню.</p>

        </div>
    </div>
</div>
</section>