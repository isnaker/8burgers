<div id="page-delivery" >
    <div class="container">
        <div class="row">
            <div class="mb-xlg">
                <h1 class="h1 font-opensans text-center mb-lg">Доставка и оплата</h1>

                <div class="col-md-5 col-md-offset-1">
                    <p class="h3 text-uppercase font-opensans mb-lg">время работы</p>
                    <p class="h4">Мы работаем каждый день с 11-00 до 22-00,
                        Пятница и Суббота с 11-00 до 01-00</p>
                </div>

                <div class="col-md-5">
                    <p class="h3 text-uppercase font-opensans mb-lg"> доставка</p>
                    <p class="h4">Доставка – БЕСПЛАТНО домой или в офис по Москве
                        в пределах МКАД</p>
                    <p class="h4">Время доставки ЮВАО – 1 час</p>

                    <p class="h4">Остальные районы 1.5 часа при условии пешей доступности
                        от метро.</p>

                    <p class="h4">Наши операторы озвучат вам точное время доставки
                        до Вашего адреса.</p>
                </div>
            </div>
        </div>
    </div>


    <section class="delivery_min_summ">
                <div class="container-fluid">
                <div class="col-md-12">
                    <p class="h4 text-center font-opensans">
                        Минимальная сумма доставки: <strong class="font-opensans">
                            600 руб.
                        </strong>
                    </p>
                </div>
            </div>
    </section>

    <div class="container">
                <div class="col-md-5">
                    <div class="alert alert-info">
                        <img src="/img/pineco.png" class="img-responsive center-block">
                        <p class="h4">
                            Мы используем упаковку PICNECO
                            из сахарного тростника, кукурузного крахмала и соломы пшеницы.
                            Это современный экопродукт, созданный специально для того,
                            чтобы сохранить Ваше здоровье и окружающую среду.
                        </p>
                    </div>
                </div>

                <div class="col-md-7">
                    <p class="h2 text-uppercase font-opensans mt-none"> Упаковка</p>
                    <p class="h4">Упаковка включает в себя один слой бумаги и два слоя фольги.
                        За счет этого Вы сможете получить Ваши бургеры горячими и сочными, но,
                        в то же время, булочки не заберут воду и останутся мягкими и вкусными.</p>
                    <p class="h4">Доставка производится в специальных термосумках,
                        сохраняющих тепло и свежесть.
                    </p>
                </div>

    </div>
</div>