<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.05.2016
 * Time: 18:52
 */

?>



<section class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-home"></i> </a></li>
                    <li><a href="/menu/">Меню</a></li>
                    <?php if (isset($_GET['category'])) { ?>
                        <li class="active"><?php print $categoryName; ?></li>
                    <?php } ?>
                </ol>
            </div>
        </div>
    </div>
</section>

<section id="page-category">


        <div class="promo">
            <div class="text-center">
                <a class="h1 font-opensans" href="/geekburgers">
                    <span class="geek"> GEEK</span>BURGER
                    <sup class="color-yellow font-opensans">beta</sup></a>
                <p class="h5 font-opensans">Единственные бургеры с супергеройской символикой! Выбери свой!</p>
            </div>
        </div>


    <div class="container">
        <div class="row">
            <div class="category-list text-center">
                <?php if (isset($categories)) { ?>
                    <?php foreach ($categories as $item){ ?>
                        <div class="col-md-2 col-xs-6">
                            <div class="item <?php if (isset($category) && ($category->id == $item->id))  { ?> active <?php } ?>">
                                <a class="h4 text-center font-opensans" href="/menu/<?php print $item->seo_url ?>">
                                    <img src="/img/icons/<?php print $item->image; ?>"> <?php print $item->name; ?>
                                </a>
                            </div>
                        </div>
                   <?php  } ?>
                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if (isset($_GET['category'])) {  ?>
                    <p class="h1 color-white"><strong><?php print $categoryName; ?></strong></p>
                <?php } ?>
            </div>
        </div>

        <div class="row">
            <div class="products">
                  <?php if (isset($products) && count($products) > 0) { ?>
                    <?php foreach ($products as $item){

                          if (count($item->images) == 0) { // if array is empty

                              $img = new stdClass();
                              $img->image_url = "burger2.png";
                              array_push($item->images, $img);
                          };
                          ?>
                        <div class="col-md-3 col-xs-6">
                            <div class="item" data-id="<?php print $item->id; ?>">
                                <div class="responder">
                                    Добавлено в корзину!
                                </div>
                                <a class="h2 text-center text-uppercase heading" href="/menu/product/<?php print $item->seo_url ?>">
                                    <?php print $item->name; ?>
                                </a>

                                <div class="rating text-center">

                                    <div star_count="<?php print $item->rating; ?>" class="stars"><i class="fa fa-star-o"></i></div>
                                </div>

                                <a class="product-image-container" href="/menu/product/<?php print $item->seo_url ?>">
                                    <img src="/img/products/<?php print $item->images[0]->image_url; ?>"
                                         class="img-responsive center-block product-image">
                                </a>
                                <div class="product-info">
                                    <p class="h2">
                                        <span class="price font-opensans"><?php print $item->price  ?></span>  <i class="fa fa-rouble"></i>
                                        <small class="weight"> <span class="font-opensans"><?php print $item->weight  ?></span>  гр. <i class="fa fa-balance-scale"></i> </small>
                                    </p>

                                    <div class="controls clearfix">
                                        <a class="btn btn-lg btn-danger addtocart font-opensans" data-count="1" data-id="<?php print $item->id; ?>">
                                            <span class="hidden-xs"><i class="fa fa-shopping-basket"></i></span> В корзину</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php  } ?>
                <?php }  else { ?>

                        <div class="text-center"><p class="h4 color-white">Нет продуктов.</p> </div>
                 <?php } ?>
            </div>
        </div>
    </div>

</section>
