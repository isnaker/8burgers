<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11.05.2016
 * Time: 23:02
 */

include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");

$app = new App();

$input = $_POST;
$response = array();

if (isset($input['action'])){
    $response['action'] = $input['action'];

    switch ($input['action']){
        case "add_to_cart":
            Cart::add_to_cart($input['id'], $input['count']);
            $response['response'] = "add product to cart";
            break;

        case "remove_from_cart":
            Cart::removeFromCart($input['id']);
            $response['response'] = "remove product from cart";
            break;

        case "get_cart":
            $total = 0;
            $products = array();
            foreach($app->cart->products as $item) {
                $product = $app->db->getProduct($item['id']);
                // calculate overall price
                array_push($products, $product);
                $total += $product->price * (int)$item['count'];
            }
            $response['products'] = json_encode($products);
            $response['total'] = $total;
            $response['data'] = Cart::getJSON();
            $response['response'] = "retrieve cart";
            break;

        case "create_order":
            $response['response'] = $app->createOrder($input['data']);
            Cart::clear();
            break;

        case "update_order":
            $response['response'] = $app->updateOrder($input['data']);
            break;
        case "add_order":
            $response['response'] = $app->createOrderAdmin($input['data']);
            break;
        case "get_order":
            $response['data'] = $app->getOrder($input['data']['id']);
            $response['response'] = "retrieve order # ".$input['data']['id'];
            break;

        case "get_orders":
            $response['data'] = $app->getOrders($input['days']);
            break;
        case "get_couriers":
            $response['data']=$app->getCouriers();
            break;
        case "set_product_ingredients":
            $data = $input['data'];
            $response['response'] = $app->setProductIngredients($data['prod_id'],$data['ingr_id'],$data['state']);
            break;
        case "update_ingredient":
            $data = $input['data'];
            $response['response']= $app->updateIngredient($data['id'],$data['name']);
            break;
        case "delete":
            $data = $input['data'];
            $response['response']=$app->db->removeById($data['table'],$data['id']);
            break;
        case "update_courier":
            $data = $input['data'];
            if($data['free']==0){
                $state=1;
            }
            else{
                $state=0;
            }
            $query=$app->db->set(array("courier_id"),array(0),"orders","WHERE `courier_id`='".$data['id']."' AND `state_id` BETWEEN 1 AND 3 ");
            $app->db->set(array("free"),array($state),"courier","WHERE `id`='".$data['id']."'");
            $response['response']= $query;
            break;
        case "get_products":
            $products=$app->db->get("*","products","");
            foreach ($products as $item){
                $img=$app->db->get("*","products_images","WHERE product_id='$item->id' LIMIT 1");
                $item->img = $img->image_url;
            }
            $response['data'] = json_encode($products);
            break;
        case "getLogs":
            $response['data']=$app->getLogs($input['data']);
            break;
    }
    print json_encode($response);
}