<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 29.04.2016
 * Time: 12:39
 */

const IS_HOLIDAY = 0;

const PAGE_HOME = 001;
const PAGE_MENU = 002;
const PAGE_PRODUCT = 003;
const PAGE_CONTACTS = 004;
const PAGE_DELIVERY = 005;
const PAGE_ACTIONS = 006;

include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/DB.php");
include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/Cart.php");

class App {
    public static $can_send_sms = 1;
    public  $db;
    public  $cart;
    public static $routes = array(
        array("name" => "Главная", "url" => "/", "content" => "<i class='fa fa-home'></i>", "page_num" => PAGE_HOME),
        array("name" => "Меню", "url" => "/menu/", "content" => "Меню", "page_num" => PAGE_MENU),
       // array("name" => "Акции", "url" => "/actions/","content" => "Акции","page_num" => PAGE_ACTIONS),
        array("name" => "Доставка", "url" => "/delivery/", "content" => "Доставка и оплата", "page_num" => PAGE_DELIVERY),
        array("name" => "Контакты", "url" => "/contacts/", "content" => "Контакты", "page_num" => PAGE_CONTACTS),
   );

    public function __construct()
    {
        session_start();
        $this->db = new DB();
        $this->cart = new Cart();
    }

    public  static function routes() {
        return self::$routes;
    }

    public function createLog($order_id,$comment){
        return $this->db->add(
            array('date','comment','order_id'),
            array(date('Y-m-d H:i:s'),$comment,$order_id),
            'logs');
    }
    public function createOrderAdmin($data){
        $order=json_decode($data);
        // add client first
        $this->db->add(
            array('name','phone','address'),
            array($order->client->name,$order->client->phone,$order->client->address),
            'clients');
        // add order

        $client_id = $this->db->getLastId();
        $this->db->add(
            array('client_id','contents','date_created','state_id','comment'),
            array($client_id,$order->contents, date('Y-m-d H:i:s'), 1,$order->comment),
            'orders'
        );

        $last_order_id=$this->db->getLastId();
        //add log
        $this->createLog($last_order_id,"Заказ создан Админом");
        return json_encode($last_order_id);
    }
    public function createOrder($data){
        // add client first
        $this->db->add(
            array('name','phone','address'),
            array($data['name'],$data['phone'],$data['address']),
            'clients');

        $client_id = $this->db->getLastId();

       // collect products
        $products = array();
        foreach ($this->cart->products as $item){
            $product = $this->db->getProduct($item['id']);
            $product->count = $item['count'];
            array_push($products, $product);
        }

        // add order
        $this->db->add(
            array('client_id','contents','date_created','state_id','comment'),
            array($client_id,json_encode($products, JSON_UNESCAPED_UNICODE), date('Y-m-d H:i:s'), 1,$data['comment']),
            'orders'
        );
        $last_order_id=$this->db->getLastId();
        //add log
        $this->createLog($last_order_id,"Заказ создан");

        if (App::$can_send_sms == 1){
            $phone = $data['phone'];
            $phone = str_replace("+","", $phone);
            $phone = str_replace("(","", $phone);
            $phone = str_replace(")","", $phone);
            $phone = str_replace(" ","", $phone);
            $phone = str_replace("-","", $phone);
            $this->sendSMS($phone, "Ваш заказ №$last_order_id сформирован. Пожалуйста, ожидайте.");
        }


        return $last_order_id;
    }

    public function getOrder($id){
        $order = $this->db->get('*','orders'," WHERE `id` = '".$id."'");
        //get client
        $client = $this->db->get('*', 'clients', " WHERE `id` = '".$order->client_id."'");
        //get state
        $state = $this->db->get('*', 'order_states', " WHERE `id` = '$order->state_id' ");


        $order->client = $client;
        $order->state = $state;
        return json_encode($order, JSON_UNESCAPED_UNICODE);
    }

    public function getOrders($days=1){
        $date=date("Y-m-d H:i:s",time()-60*60*24*$days);
        $orders = $this->db->get('*,DATE_FORMAT(date_created,\'%H:%i:%s %d.%m.%Y\') as date_created','orders',"WHERE `date_created`>='".$date."' ORDER BY state_id ASC, date_created DESC");
        if(!is_array($orders)){$orders=array($orders);}

        foreach ($orders as $item){
            $client = $this->db->get('*', 'clients', " WHERE `id` = '".$item->client_id."'");
            $state = $this->db->get('*', 'order_states', " WHERE `id` = '$item->state_id' ");
            $item->state = $state;
            $item->client = $client;
        }

        return json_encode($orders, JSON_UNESCAPED_UNICODE);
    }
    public function setProductIngredients($prod_id,$ingr_id,$state){
        if($state==1){
           $query = $this->db->remove("product_ingredients","WHERE `product_id`='".$prod_id."' AND `ingredient_id`='".$ingr_id."'");
        }
        else{
           $query = $this->db->add(array("product_id","ingredient_id"),array($prod_id,$ingr_id),"product_ingredients","");
        }
        return $query;
    }

    public function updateOrder($data){
        $order=json_decode($data);
        //set client info
        //set order info
        //$order->contents=json_decode($order->contents);

        $old_order=$this->db->get("*","orders","WHERE `id`='".$order->id."'");//Берем информацию о старом заказе
        $old_courier=$this->db->get("*","courier","WHERE `id`='".$old_order->courier_id."'");//Берем информацию о старом курьере
        $this->db->set(array("free"),array(1),"courier","WHERE `id`='".$old_courier->id."'");//Освобождаем старого курьера

        $this->db->set(array("courier_id"),array($order->courier_id),"orders","WHERE `id`='".$order->id."'");//Назначаем нового курьера
        $this->db->set(array("free"),array(0),"courier","WHERE `id`='".$order->courier_id."'");//Присваевам статус "занят" новому курьеру

        if($order->state_id==4 || $order->state_id==5){
            $this->db->set(array("free"),array(1),"courier","WHERE `id`='".$order->courier_id."'");//Освобождаем курьера если заказ выполнен
        }

        $names_array=array("comment","courier_id","state_id","contents","sdacha","time_travel","time_to","time_cook");
        $values_array=array($order->comment,$order->courier_id,$order->state_id,$order->contents,$order->sdacha,$order->time_travel,$order->time_to,$order->time_cook);
        $this->db->set($names_array,$values_array,"orders","WHERE `id`=".$order->id);

        $names_array=array("name","phone","address");
        $values_array=array($order->client->name,$order->client->phone,$order->client->address);
        $this->db->set($names_array,$values_array,"clients","WHERE `id`=".$order->client_id);

        //create log
        $log="";
        $new_order=$this->db->get("*","orders","WHERE `id`='".$order->id."'");//Берем информацию о новом заказе
        if($old_order->state_id!=$new_order->state_id){
            $old_state=$this->db->get("*","order_states","WHERE `id`='$old_order->state_id'");
            $new_state=$this->db->get("*","order_states","WHERE `id`='$new_order->state_id'");
            $log.="Изменение статуса заказа с $old_state->name на $new_state->name";
            $log=$this->createLog($order->id,$log);
        }
        if($new_order->contents!=$old_order->contents){
            $log=$this->createLog($order->id,"Состав заказа был изменен");
        }

        //return json_encode($log,JSON_UNESCAPED_UNICODE);
        return $log;
    }
    public function getCouriers(){
        $couriers = $this->db->get('*','courier','');
        return json_encode($couriers);
    }
    public function updateIngredient($id,$name){
       return $this->db->set(array("name"),array($name),"ingredients","WHERE `id`='".$id."'");
    }

    public function getLogs($order_id){
        $logs = $this->db->get('*','logs',"WHERE order_id='$order_id' ORDER BY `date` DESC");
        if(!is_array($logs)){
            $logs=array($logs);
        }
        return json_encode($logs,JSON_UNESCAPED_UNICODE);
    }
    public function updateClient($data){

    }

    public function sendSMS($to,$message){
        $ch = curl_init("http://sms.ru/sms/send");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            "from"          => "8burgers",
            "api_id"		=>	"DB3F0021-7BBE-81B2-266D-2ED6FD220ECB",
            "to"			=>	$to,
            "text"		=>	iconv("utf-8","utf-8",$message)

        ));
        $body = curl_exec($ch);
        curl_close($ch);
    }
}