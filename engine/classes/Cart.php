<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11.05.2016
 * Time: 22:59
 */

class Cart {
    public $products = array();

    public function __construct(){
        if (isset($_SESSION['cart'])){
            $this->products = ($_SESSION['cart']);
        } else {
            $_SESSION['cart'] = array();
        }
    }

    public static function add_to_cart($id,$count = 1){
        $needtoadd = true;

        if ( (int)count($_SESSION['cart']) > 0){
            // check if there are already added products
            for ($i=0; $i < count($_SESSION['cart']); $i++){
                $item = $_SESSION['cart'][$i];

                if ( (int)$item['id'] == (int) $id){
                    // !dublicate!
                    $product['id'] = (int)$id;
                    $product['count'] = $_SESSION['cart'][$i]['count'] + (int)$count;
                    $_SESSION['cart'][$i] = $product;
                    if ($product['count'] <= 0){ self::removeFromCart($id); }
                    $needtoadd = false;
                }
            }
        }

        if ($needtoadd){
            $product['id'] = (int)$id;
            $product['count'] = (int)$count;
            array_push($_SESSION['cart'], $product);
        }
    }

    public static function removeFromCart($productId){
            // check if there are already added products
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                $item = $_SESSION['cart'][$i];
                if ( (int)$item['id'] == (int) $productId){
                    array_splice($_SESSION['cart'], $i, 1);
                }
            }

    }

    public static function getJSON(){
        return (($_SESSION['cart']));
    }

    public function validate(){

    }

    public static function clear(){
        unset($_SESSION['cart']);
        $_SESSION['cart'] = array();
    }
}

if (isset($_GET['clear'])){
    session_start();
    Cart::clear();
}