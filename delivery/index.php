<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.06.2016
 * Time: 11:05
 */


include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");
$app = new App();

$includable = array("/engine/parts/pages/delivery.php");
$current_page = PAGE_DELIVERY;
include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/page_template.php");

?>