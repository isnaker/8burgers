<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.05.2016
 * Time: 18:51
 */
include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");
$app = new App();

$categories = $app->db->get("*", "categories", "WHERE `parent_id` = '0' AND `inactive` = 0");



$filter = "";

if (isset($_GET['category'])){
    $id = $_GET['category'];

    $category = $app->db->get('*', 'categories', " WHERE `seo_url` = '$id' LIMIT 1");
    $categoryName = $app->db->getCategoryNameById($category->id);
    $products = $app->db->getProductsFromCategory(array($category->id));
} else {
    $products = $app->db->getProduct("*");
}


$includable = array("/engine/parts/pages/category.php");
$current_page = PAGE_MENU;
//$headSlider = file_get_contents($_SERVER['DOCUMENT_ROOT']."/engine/parts/head_slider.php");
include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/page_template.php");