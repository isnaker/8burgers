<?php

    include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");
    $app = new App();


    //set mode!
    if (isset($_GET['mode'])){
        switch ($_GET['mode']){
            case MODE_8BURGERS:
                $_SESSION['mode'] = MODE_8BURGERS; break;
            case MODE_GEEKBURGER:
                $_SESSION['mode'] = MODE_GEEKBURGER; break;
        }
        header("Location: /");
    }

    $categories = $app->db->get("*", "categories", "WHERE `parent_id` = '0'");

    if ($app->mode == MODE_8BURGERS){
        $products = $app->db->getProduct(array(1,2,3,4));
        $headSlider = file_get_contents($_SERVER['DOCUMENT_ROOT']."/engine/parts/head_slider.php");
        $includable    = array("/engine/parts/pages/home.php");
        $current_page = PAGE_HOME;
        include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/page_template.php");
    } else {
        $current_page = PAGE_HOME;
        $products = $app->db->getProduct(array(1,2,3,4,5,6,7,8));
        $headSlider = file_get_contents($_SERVER['DOCUMENT_ROOT']."/engine/parts/slider_geekburger.php");
        $includable    = array("/engine/parts/pages/home.php");
        include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/page_template_geekburger.php");
    }

?>