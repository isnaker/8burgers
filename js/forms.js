/**
 * Created by Alex on 28.03.2016.
 */
$(function(){
    $('.sender').click(function(){
        var validated = true;
        var thus = this;
        var parent = $( $(this).attr('data-parent') );
        var fields = $(parent).find('.input');
        var message = "<h3>" + $(this).attr('data-heading') + "</h3>";

        if ($(fields).val() == ""){ validated = false; }

        if (validated){
            for (var i=0; i < fields.length; i++){
                message += $(fields[i]).attr('data-label') + ": <strong>"+$(fields[i]).val()+"</strong><br>";
            }

            $.ajax({
                type: 'POST',
                url: '/engine/ajax.php',
                data: {
                    action: "callback",
                    message: message
                },
                success: function () {
                    $(parent).find('.responder').html("Сообщение успешно отправлено!")
                        .addClass('alert-success')
                        .removeClass('alert-danger')
                        .removeClass('hidden');

                    $(thus).hide();
                },
                timeout: function () {
                    _response = 0;
                },
                error: function () {
                    _response = 0;
                }
            });


        } else {
            $(fields).parent().addClass('has-error');

            $(parent).find('.responder').html("Ошибка! Вы не ввели номер телефона!")
                .addClass('alert-danger')
                .removeClass('alert-success')
                .removeClass('hidden');
        }



    });
});