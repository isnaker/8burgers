/**
 * Created by Alex on 25.03.2016.
 */
$(function(){
    $('.slick-slider').slick(
    {
        dots: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssTransitions: true,
        minHeight: 850,
        responsive: [
        {
            breakpoint: 1024,
            settings: {

            }
        },
        {
            breakpoint: 600,
            settings: {

            }
        },
        {
            breakpoint: 480,
            settings: {
                arrows: false
            }
        }
    ]
    });
});