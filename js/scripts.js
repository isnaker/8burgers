
$(function(){

    var stars=$('.stars');
    for(var i=0;i<stars.length;i++){
        $(stars[i]).html(set_stars(stars[i]));
    }


    function set_stars(stars) {
        var count = $(stars).attr('star_count');//постоянное значение
        var str = "";
        var full_star = "<i class='fa fa-star' aria-hidden='true'></i> ";
        var half_star = "<i class='fa fa-star-half-o' aria-hidden='true'></i> ";
        var void_star = "<i class='fa fa-star-o' aria-hidden='true'></i> ";
        for(var i=0; i<5;i++){
            if (count > 1) {
                str += full_star;
            }
            else if (count == 1) {
                str += half_star;

            }
            else if (count <= 0) {
                str += void_star;
            }
            count -= 2;
        }

        return str;
    }

    $('input[type="tel"]').mask('+7 (999) 999-99-99');
    $('input').focus(function(){
        $(this).parent().removeClass('has-error');
    });

    $(window).scroll(function(){
        var scroll = $(window).scrollTop();

        if (scroll > 290){
            $('#stick_navigation').addClass('fixed');
        } else {
            $('#stick_navigation').removeClass('fixed');
        }
    });

    //jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function() {
        $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 50
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });

    $('.fancybox').fancybox();

    new WOW().init();

    var h = $(window).height() - ( $('#promo-content').height() + $('#stick_navigation').height() ) ;
    $('#geek-container').css('height', h + "px");

});


//ymaps.ready(init);
var myMap,
    myPlacemark;

function init() {
    myMap = new ymaps.Map("map", {
        center: [55.702881,37.772076],
        zoom: 14
    });
}