/**
 * Created by Alex on 24.03.2016.
 */
$(function(){
    for (var i=0; i < products.length; i++){
        var item = products[i];

        var sub = "<ul class='list-inline'>";
        for (var j=0; j < item.sub.length; j++){
            sub+= "<li><button class='btn btn-default btn-sm category_switcher' data-value-id='"+item.sub[j].id+"'>" + item.sub[j].name + "</button></li>";
        }
        sub+="</ul>";

        //var cat = "";
        //if (i == 0) {cat = "active"} else {cat = "";}
        $('[data-value="product_categories"]')
            .append("<div class='col-md-4'><p class='h3 product-category'><span>"+item.categoryName+"</span></p>"+sub+"</div>");
    }

    //display product
    displayCategory(loadCategory(0));
    //display product
    displayProduct(loadProduct(1));



    $('.category_switcher').unbind().click(function(){
        var id = parseInt($(this).attr('data-value-id'));
        displayCategory(loadCategory(id));
    });
});

function loadCategory(id){
    var foundItem = undefined;
    for (var i=0; i<products.length; i++){
        for (var j=0; j<products[i].sub.length; j++) {
            if (products[i].sub[j].id == id) {
                foundItem = products[i].sub[j];
            }
        }
    }
    return foundItem;
}

function displayCategory(category){
    var item_ = category;
    $('[data-value="product_models"]').html("");

        for (var l=0; l<item_.products.length; l++){
            var product = item_.products[l];
            $('[data-value="product_models"]').append("<li data-value-id='"+product.id+"' class='model_switcher'><button class='btn btn-default'><strong>"+product.model+"</strong></button></li>");
        }

    displayProduct(item_.products[0]);

    $('.model_switcher').unbind().click(function(){
        var id = parseInt($(this).attr('data-value-id'));
        displayProduct(loadProduct(id));
    });
}

function loadProduct(id)
{
    var foundProduct = undefined;

    for (var i=0; i<products.length; i++){
        for (var j=0; j<products[i].sub.length; j++){
            for (var l=0; l<products[i].sub[j].products.length; l++){
                if (products[i].sub[j].products[l].id == id){
                    foundProduct = products[i].sub[j].products[l];
                }
            }
        }
    }
    return foundProduct;
}

function displayProduct(product){
    $("[data-value='product_name']").html("").html(product.model);
   // $("[data-value='product_price']").html("Уто");
    $("[data-value='product_contents']").html("").html(product.contents);
    $("[data-value='product_description']").html("").html(product.description);

    if ( product.images[0] == undefined) {product.images[0] = "logo.png"}

    $("[data-value='product_img']").attr('src', "/img/products/" + product.images[0]);

   // var height = $( $("[data-value='product_img']")).parent().parent().height();
   // var image_height = $("[data-value='product_img']").height();

    //$("[data-value='product_img']").css('marginTop','0px').css('marginTop', ((height -image_height) / 2 )+"px");
}