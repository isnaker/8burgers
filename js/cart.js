/**
 * Created by Alex on 11.05.2016.
 */
$(function(){


    getCart();
    $('.products .addtocart').click(function(){
        var responder = $(this).parent().parent().parent().find('.responder');
        console.log(responder);
        addToCart(this, responder);
    });

    $('#page-product .addtocart').click(function(){
        var responder = $('#page-product .image').find('.responder');
        addToCart(this, responder);
    });

    $('#create_order').click(function(){
        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'create_order',
                data: {
                    name: "Клиент с сайта",
                    phone: $('[data-value="phone"]').val(),
                    email: "",
                    address:  $('[data-value="address"]').val() + "  , дом/офис: " + $('[data-value="apartment"]').val(),
                    comment: $('[data-value="comment"]').val()
                }
            },
            success: function(response){
                var resp = JSON.parse(response);
                if (resp.response != -1){
                    $('.cart-order-form').hide(500);
                    $('#cart-contents').html(
                        '<p class="h2 text-center font-opensans">Ваш заказ успешно создан! Номер вашего заказа: ' +
                        resp.response + "</p>");
                }
            }
        });
    });

    $('#add_comment_switcher').click(function(){
        $('[data-value="comment"]').toggleClass('hidden');
    })

});

function addToCart(caller, responder){
    var thus = caller;
    var id = $(thus).attr('data-id');

    var count = 1;
    if ( parseInt($(thus).attr('data-count')) != 0){
        count = parseInt($(thus).attr('data-count'));
    } else {
        count = 1;
    }

    $.ajax({
        method: "POST",
        url: "/engine/classes/Ajax.php",
        data: {
            action: 'add_to_cart',
            id: id,
            count: count
        },
        success: function(){
            $(responder).addClass('active');
            setTimeout( function(){ $(responder).removeClass('active'); }, 2500);
            getCart();
        }
    });
}

function getCart(){
    $.ajax({
        method: "POST",
        url: "/engine/classes/Ajax.php",
        data: {
            action: 'get_cart'
        },
        success: function(e){
            var res = JSON.parse(e);
            $('[data-value="cart-count"]').html(res.data.length);
            //set cart value total
            $('[data-value="data-cart-total"]').html(res.total);

            if (parseInt(res.total) < 600){
                $('#cart-controls').addClass('hidden');
                $('#cart-too-few-alert').removeClass('hidden');
            } else {
                $('#cart-controls').removeClass('hidden');
                $('#cart-too-few-alert').addClass('hidden');
            }

            //print cart;
            printCart(res);
        }
    });
}

function printCart(cart){
    var products = (JSON.parse(cart.products));
    var container = $("[data-value='cart-contents']");

    //loop products
    $(container).html("");

    for (var i=0; i<(products.length); i++ ){
        var product = products[i];
        var count = cart.data[i].count;
        var info="";
        if(product.category.id==1){

            info="<p class='h5 font-opensans price color-yellow'> + Картофель фри в подарок!</p>"
        }
        if(product.id==12 || product.id==13){
            info="<p class='h5 font-opensans price color-yellow'> + Соус на выбор в подарок!</p>"
        }

        $(container).append(
            "<div class='col-md-4 col-xs-6'>" +
            "<div class='cart-item text-center'>" +
            "<i class='fa fa-close pull-right removeFromCart' data-id='"+product.id+"'></i>"+
            "<img src='/img/products/"+product.images[0].image_url+"' class='img-responsive'>"+

            "<p class='h3 font-opensans product-name'>"+product.name+"</p>"+
            "<p class='h4 font-opensans price'><strong>"+product.price+"</strong> <i class='fa fa-rouble'></i> </p>"+
            info+
            "<hr>"+

            "<p class='h4'>"+
            "<i title='Уменьшить' class='fa fa-minus-square-o fa-2x cart-changer' data-id='"+product.id+">' data-count='-1'></i>"+
            " <strong data-product-count='"+product.id+"' class='count'> "+
            "<span class='font-opensans'>"+count+"</span > шт. "+
            "</strong> "+
            "<i  title='Увеличить' class='fa fa-plus-square-o fa-2x cart-changer' data-id='"+product.id+"' data-count='1'></i>"+
            "</p>"+
            "</div>"+
            "</div>");
    }


    $('.cart-changer').click(function(){
        var thus = this;
        var id = $(this).attr('data-id');
        var count = 1;
        if ( parseInt($(thus).attr('data-count')) != 0){
            count = parseInt($(thus).attr('data-count'));
        } else { count = 1; }

        $(thus).parent().find('.count').html('<i class="fa fa-spin fa-spinner"></i>');
        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: {
                action: 'add_to_cart',
                id: id,
                count: count
            },
            success: function(){
                getCart();
            }
        });


    });

    $('.removeFromCart').click(function(){
        var id = $(this).attr('data-id');
        $.ajax({
            method: "POST",
            url: "/engine/classes/Ajax.php",
            data: { action: 'remove_from_cart', id: id },
            success: function(){ window.location.reload(); }
        });
    });

}