<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 12.07.2016
 * Time: 15:21
 */


include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");
$app = new App();

$addictional_css = array("/css/geekburgers/promo.css");
$includable    = array("/engine/parts/pages/geekburgers.php");
$current_page = PAGE_HOME;
include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/page_template_geekburgers_promo.php");